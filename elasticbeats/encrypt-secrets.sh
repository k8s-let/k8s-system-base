#!/usr/bin/bash
#
# Credentials have to be added PER CLUSTER: encryption uses cluster specific private key!
#

ELASTIC_CLOUD_ID=replaceMe
ELASTIC_CLOUD_AUTH=replaceMe

SECRET_NAME=elastic-credentials
SECRET_FILE=${SECRET_NAME}.yml

kubectl create secret generic --from-literal=ELASTIC_CLOUD_ID=${ELASTIC_CLOUD_ID} --from-literal=ELASTIC_CLOUD_AUTH=${ELASTIC_CLOUD_AUTH}  --dry-run ${SECRET_NAME} -o json >private_${SECRET_FILE}
kubeseal --format=yaml <private_${SECRET_FILE} -n sealed-secrets --controller-namespace=local-system --namespace=local-system --scope=namespace-wide >${SECRET_FILE}
