# k8s-system-base

Basic (cluster implementation agnostic) part of system configuration for clusterwide configuration objects. These blueprints can be used as kustomization bases in cluster specific repositories. 

## Kustomization

Cluster components are arranged in subdirectories. Each subdirectory must contain a `kustomization.yml` file. `kustomize` uses multiple YAML files and merges them into a single YAML, with the option to change or add parts.

[kustomize site](https://kubernetes-sigs.github.io/kustomize/)
[kustomize reference](https://kubectl.docs.kubernetes.io/references/kustomize/)

## Usage

Create a git repository for a specific cluster. Add this repository as a git submodule:

```
git submodule add https://gitlab.ethz.ch/k8s-let/k8s-system-base.git k8s-system-base-unstable
git submodule add -b feature https://gitlab.ethz.ch/k8s-let/k8s-system-base.git k8s-system-base-feature

```
Now the submodules can be used in a `kustomization.yml`:

```
resources:
  - ./k8s-system-base-feature/ingress-nginx
  - my-additional-declaration.yml
```

